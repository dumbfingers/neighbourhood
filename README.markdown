# Project Setup


## Setup
Please prepare your `APP_ID` and `APP_CODE` and put them into `gradle.properties` file in this format:
```
#HERE demo API keys
APP_ID=DemoAppIdxxxxxxxxxxx
APP_CODE=AJxxxxxxxxxxxxxxx
```
**Please Note**:

In order not to leak your `APP_ID` and `APP_CODE` into public, please use a `gralde.properties` file that outside your repo.
For example, you can use the one located in `~/.gradle/gradle.properties` (you can create a new one if it doesn't exist)

## Dependencies
The libraries used are listed below which you can also find in the module's `build.gralde` file:

```
    //Butterknife
    compile 'com.jakewharton:butterknife:8.5.1'

    //RxJava and RxAndroid
    compile 'io.reactivex.rxjava2:rxandroid:2.0.1'
    compile 'io.reactivex.rxjava2:rxjava:2.0.1'
    //Retrofit
    compile 'com.squareup.retrofit2:retrofit:2.1.0'
    compile 'com.jakewharton.retrofit:retrofit2-rxjava2-adapter:1.0.0'
    compile 'com.squareup.retrofit2:converter-gson:2.1.0'
    //Play services
    compile 'com.google.android.gms:play-services-location:10.0.1'
    compile 'com.google.android.gms:play-services-maps:10.0.1'
    //Android Support Lib
    compile 'com.android.support:appcompat-v7:25.1.1'
```