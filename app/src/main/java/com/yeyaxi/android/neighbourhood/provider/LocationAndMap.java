package com.yeyaxi.android.neighbourhood.provider;

import android.location.Location;

import com.google.android.gms.maps.GoogleMap;

public class LocationAndMap {

    private GoogleMap googleMap;
    private Location location;

    public LocationAndMap(Location location, GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.location = location;
    }

    public GoogleMap getGoogleMap() {
        return googleMap;
    }

    public void setGoogleMap(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
