package com.yeyaxi.android.neighbourhood.provider;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import java.lang.ref.WeakReference;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

public class GoogleMapInitialisationManager {

    WeakReference<SupportMapFragment> fragmentRef;

    public GoogleMapInitialisationManager(SupportMapFragment fragment) {
        this.fragmentRef = new WeakReference<>(fragment);
    }

    public Observable<GoogleMap> observeMapReady() {
        return Observable.create(new ObservableOnSubscribe<GoogleMap>() {
            @Override
            public void subscribe(final ObservableEmitter<GoogleMap> emitter) throws Exception {
                final OnMapReadyCallback onMapReadyCallback = new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        emitter.onNext(googleMap);
                    }
                };

                if (fragmentRef.get() == null) {
                    return;
                }

                SupportMapFragment mapFragment = fragmentRef.get();
                mapFragment.getMapAsync(onMapReadyCallback);
            }
        });
    }
}
