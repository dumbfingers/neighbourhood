package com.yeyaxi.android.neighbourhood.model;

import com.google.gson.annotations.SerializedName;

public class QueryResponse {

    @SerializedName("results")
    private Results results;

    @SerializedName("search")
    private Search search;

    public Results getResults() {
        return results;
    }

    public void setResults(Results results) {
        this.results = results;
    }

    public Search getSearch() {
        return search;
    }

    public void setSearch(Search search) {
        this.search = search;
    }
}
