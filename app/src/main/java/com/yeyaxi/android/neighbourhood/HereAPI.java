package com.yeyaxi.android.neighbourhood;

import com.yeyaxi.android.neighbourhood.model.QueryResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface HereAPI {

    String BASE_URL = "https://places.cit.api.here.com/places/v1/";
    String APP_ID = BuildConfig.APP_ID;
    String APP_CODE = BuildConfig.APP_CODE;

    @GET("discover/search?app_id="+ APP_ID + "&app_code=" + APP_CODE)
    Observable<QueryResponse> getPlaces(@Query("at") String location, @Query("q") String keyword);
}
