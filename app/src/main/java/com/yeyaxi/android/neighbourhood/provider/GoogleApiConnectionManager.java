package com.yeyaxi.android.neighbourhood.provider;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresPermission;
import android.test.suitebuilder.annotation.Suppress;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.lang.ref.WeakReference;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

public class GoogleApiConnectionManager {

    WeakReference<GoogleApiClient> googleApiClientRef;

    public GoogleApiConnectionManager(GoogleApiClient googleApiClient) {
        this.googleApiClientRef = new WeakReference<>(googleApiClient);
    }

    public Observable<Location> observeGoogleApiConnection() {
        return Observable.create(new ObservableOnSubscribe<Location>() {
            @Override
            public void subscribe(final ObservableEmitter<Location> emitter) throws Exception {
                if (googleApiClientRef.get() == null) {
                    return;
                }

                final GoogleApiClient googleApiClient = googleApiClientRef.get();

                final GoogleApiClient.ConnectionCallbacks connectionCallbacks = new GoogleApiClient.ConnectionCallbacks() {
                     @Override
                     public void onConnected(@Nullable Bundle bundle) {
                         Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                         emitter.onNext(location);
                     }

                     @Override
                     public void onConnectionSuspended(int i) {
                        emitter.onError(new Error("Connection Suspended"));
                     }
                 };

                GoogleApiClient.OnConnectionFailedListener connectionFailedListener = new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        emitter.onError(new Error(connectionResult.getErrorMessage()));
                    }
                };

                googleApiClient.registerConnectionCallbacks(connectionCallbacks);
                googleApiClient.registerConnectionFailedListener(connectionFailedListener);
            }
        });
    }
}
