package com.yeyaxi.android.neighbourhood;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HereApiClient {

    private HereAPI hereAPI;

    public HereApiClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(HereAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        hereAPI = retrofit.create(HereAPI.class);
    }

    public HereAPI getHereAPI() {
        return hereAPI;
    }
}
