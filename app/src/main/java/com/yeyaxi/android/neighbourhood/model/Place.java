package com.yeyaxi.android.neighbourhood.model;

import com.yeyaxi.android.neighbourhood.attribute.Category;
import com.yeyaxi.android.neighbourhood.attribute.OpeningHours;
import com.yeyaxi.android.neighbourhood.attribute.TransitLines;

import java.util.List;

public class Place {

    private Double[] position;
    private Double[] bbox;
    private Integer distance;
    private String title;
    private Double avgRating;
    private Category category;
    private List<Category> categories;
    private String iconUri;
    private String vicinity;
    private String type; //TODO maybe it is an enum
    private String href;
    private Boolean sponsored;
    private Boolean distant;
    private String id;
    private String references;
    private TransitLines transitLines;
    private OpeningHours openingHours;

    public Double[] getPosition() {
        return this.position;
    }

    public void setPosition(Double[] position) {
        this.position = position;
    }

    public Double[] getBbox() {
        return bbox;
    }

    public void setBbox(Double[] bbox) {
        this.bbox = bbox;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(Double avgRating) {
        this.avgRating = avgRating;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public String getIconUri() {
        return iconUri;
    }

    public void setIconUri(String iconUri) {
        this.iconUri = iconUri;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public Boolean getSponsored() {
        return sponsored;
    }

    public void setSponsored(Boolean sponsored) {
        this.sponsored = sponsored;
    }

    public Boolean getDistant() {
        return distant;
    }

    public void setDistant(Boolean distant) {
        this.distant = distant;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReferences() {
        return references;
    }

    public void setReferences(String references) {
        this.references = references;
    }

    public TransitLines getTransitLines() {
        return transitLines;
    }

    public void setTransitLines(TransitLines transitLines) {
        this.transitLines = transitLines;
    }

    public OpeningHours getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(OpeningHours openingHours) {
        this.openingHours = openingHours;
    }
}
