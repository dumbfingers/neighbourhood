package com.yeyaxi.android.neighbourhood.attribute;

public class Coordinate {
    private double latitude;
    private double longitude;

    public Coordinate(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Coordinate(Double[] position) {
        if (position == null) {
            this.latitude = -1;
            this.longitude = -1;
        }
        this.latitude = position[0];
        this.longitude = position[1];
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Double[] getCoordinate() {
        return new Double[]{this.latitude, this.longitude};
    }
}
