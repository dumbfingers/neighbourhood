package com.yeyaxi.android.neighbourhood;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.yeyaxi.android.neighbourhood.model.Place;
import com.yeyaxi.android.neighbourhood.model.QueryResponse;
import com.yeyaxi.android.neighbourhood.provider.GoogleApiConnectionManager;
import com.yeyaxi.android.neighbourhood.provider.GoogleMapInitialisationManager;
import com.yeyaxi.android.neighbourhood.provider.LocationAndMap;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnEditorAction;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private final String LOG_TAG = "Neighbourhood";
    private final int LOCATION_PERMISSION_REQUEST = 0xA;
    private final int MAP_DEFAULT_ZOOM_LEVEL = 13;
    private final int MAX_RESULT_ON_MAP = 5;

    private HereApiClient hereApiClient;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private GoogleMap mMap;

    private Observable<Location> googleApiObservble;
    private Observable<GoogleMap> googleMapObservable;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.input_search)
    EditText inputSearch;
    @BindView(R.id.text_status)
    TextView statusView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST);
            return;
        }

        initGoogleApiClient();
        initMap();
        showCurrentLocation();
    }

    @Override
    protected void onStart() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        hereApiClient = getHereApiClient();
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    @SuppressWarnings({"MissingPermission"})
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST: {
                boolean granted = (grantResults.length >= 2 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED);
                setSearchEnabled(granted);
                if (granted) {
                    initGoogleApiClient();
                    initMap();
                    showCurrentLocation();
                }
                return;
            }
        }
    }

    @OnEditorAction(R.id.input_search)
    public boolean onActionSearch(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            this.onSearch();
            return true;
        }
        return false;
    }

    public void onSearch() {

        if (mLastLocation == null || inputSearch.getText().toString().trim().isEmpty() || mMap == null) {
            return;
        }

        closeKeyboard();

        // remove old markers
        mMap.clear();
        statusView.setText("");

        hereApiClient.getHereAPI()
                .getPlaces(
                        String.valueOf(mLastLocation.getLatitude()) + "," +
                                String.valueOf(mLastLocation.getLongitude()),
                        inputSearch.getText().toString())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<QueryResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        progressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onNext(QueryResponse response) {

                        if (response == null ||
                                response.getResults() == null ||
                                response.getResults().getItems() == null) {
                            return;
                        }

                        onQueryReceived(response.getResults().getItems());
                    }

                    @Override
                    public void onError(Throwable e) {
                        progressBar.setVisibility(View.GONE);
                        statusView.setText(e.toString());
                        Log.d(LOG_TAG, "onError " + e.toString());
                    }

                    @Override
                    public void onComplete() {
                        progressBar.setVisibility(View.GONE);
                        Log.d(LOG_TAG, "onComplete");
                    }
                });
    }

    private HereApiClient getHereApiClient() {
        if (hereApiClient == null) {
            hereApiClient = new HereApiClient();
        }
        return hereApiClient;
    }

    private void setSearchEnabled(boolean enabled) {
        inputSearch.setEnabled(enabled);
    }

    private void setLastLocation(Location location) {
        this.mLastLocation = location;
    }

    @SuppressWarnings({"MissingPermission"})
    private void onGoogleMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
    }

    private void initGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        GoogleApiConnectionManager googleApiConnectionManager = new GoogleApiConnectionManager(mGoogleApiClient);
        googleApiObservble = googleApiConnectionManager.observeGoogleApiConnection();
        googleApiObservble.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Location>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Location location) {
                        setLastLocation(location);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    /**
     * Known GoogleMap Issue
     * {@see <a href="https://code.google.com/p/gmaps-api-issues/issues/detail?id=11077">
     *     Issue 11077: Bug: "Suppressed StrictMode policy violation:" StrictModeDiskWriteViolation&StrictModeDiskReadViolation</a>
     *     }
     */
    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        GoogleMapInitialisationManager googleMapInitialisationManager = new GoogleMapInitialisationManager(mapFragment);
        googleMapObservable = googleMapInitialisationManager.observeMapReady();
        googleMapObservable.subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GoogleMap>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(GoogleMap googleMap) {
                        onGoogleMapReady(googleMap);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void showCurrentLocation() {
        Observable<LocationAndMap> combined = Observable.zip(googleApiObservble, googleMapObservable, new BiFunction<Location, GoogleMap, LocationAndMap>() {
            @Override
            public LocationAndMap apply(Location location, GoogleMap googleMap) throws Exception {
                return new LocationAndMap(location, googleMap);
            }
        });

        combined.subscribe(new Observer<LocationAndMap>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(LocationAndMap locationAndMap) {
                Location location = locationAndMap.getLocation();
                GoogleMap googleMap = locationAndMap.getGoogleMap();
                animateMapToLocation(location, googleMap);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void animateMapToLocation(Location location, GoogleMap googleMap) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .zoom(MAP_DEFAULT_ZOOM_LEVEL)
                .target(new LatLng(location.getLatitude(), location.getLongitude()))
                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void onQueryReceived(List<Place> places) {
        int count = places.size();
        if (count > 0) {
            statusView.setText("");
            dropMapMarkers(places.subList(0, Math.min(count, MAX_RESULT_ON_MAP)));
        } else {
            statusView.setText(getString(R.string.no_results));
        }
    }

    private void dropMapMarkers(List<Place> places) {
        for (Place place : places) {
            LatLng latLng = new LatLng(place.getPosition()[0], place.getPosition()[1]);
            mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(place.getTitle() != null ? place.getTitle() : "")
                    .snippet(place.getDistance() != null ? getString(R.string.distance_from, place.getDistance()) : "")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

            if (place.getDistance() != null && place.getDistance() > 5000) {
                Toast.makeText(this, R.string.toast_zoom_out, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
