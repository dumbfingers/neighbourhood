package com.yeyaxi.android.neighbourhood.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Results {
    @SerializedName("items")
    private List<Place> items;

    public List<Place> getItems() {
        return items;
    }

    public void setItems(List<Place> items) {
        this.items = items;
    }
}
